#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>

#define MAXIMO 256

//referencia Rafael Branganca
using namespace std;

struct pixels{
	int r,g,b; }; 

class Imagem {

	private:
		char arquivo[MAXIMO];
		string numero;
		int largura; 
		int altura;
		int escala;
		pixels** pixel;
	public:
		Imagem();
		Imagem(char arquivo[MAXIMO], string numero, int largura, int altura, int escala, pixels** pixel);
		~Imagem();
		
		char* getArquivo();
		void setArquivo(char *arquivo);

		string getNumero();
		void setNumero(string numero);

		int getLargura();
		void setLargura(int largura);

        	int getAltura();
        	void setAltura(int altura);

        	int getEscala();
        	void setEscala(int escala);

        	pixels** getPixel();
        	void setPixel(pixels** pixel);

        	void filtroPretoBranco();
        	void filtroAutoRelevo();
        	void filtroInversao();
};
#endif

