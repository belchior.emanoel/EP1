#ifndef CRIADOR_HPP
#define CRIADOR_HPP

#include <fstream>
#include "imagem.hpp"

#define MAXIMO 256

using namespace std;


class Criador{
	public:
        	char* lerNomeArquivoExistente();
        	char* lerNomeArquivoNovo();

        	void msgErro();

        	Imagem* criaImagem(char nomeArquivoExistente[MAXIMO]);

        	void escreveImagem(char nomeArquivoNovo[MAXIMO],Imagem* imagem);

};
#endif            
