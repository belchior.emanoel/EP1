#include "../inc/imagem.hpp"
#include <iostream>
#include <string>
#include <cstring>

#define MAXIMO 256

using namespace std;

Imagem::Imagem(){
        strcpy(arquivo,"");
        numero = " ";
        largura = 0;
        altura = 0;
        escala = 0;
}


Imagem::Imagem(char arquivo[MAXIMO],string numero,int largura, int altura, int escala,pixels** pixel){
        strcpy(this->arquivo, arquivo);
        this->numero = numero;
        this->largura = largura;
        this->altura = altura;
        this->escala = escala;
        this->pixel = pixel;
}

Imagem::~Imagem(){

}

char* Imagem::getArquivo(){
        return this->arquivo;
}
void Imagem::setArquivo(char* arquivo){
        strcpy(this->arquivo, arquivo);
}

string Imagem::getNumero(){
        return numero;
}
void Imagem::setNumero(string numero){
        this->numero = numero;
}

int Imagem::getLargura(){
        return largura;
}
void Imagem::setLargura(int largura){
        this->largura = largura;
}

int Imagem::getAltura(){
        return altura;
}
void Imagem::setAltura(int altura){
        this->altura = altura;
}

int Imagem::getEscala(){
        return escala;
}
void Imagem::setEscala(int escala){
        this->escala = escala;
}

pixels** Imagem::getPixel(){
        return this->pixel;
}
void Imagem::setPixel(pixels** pixel){
        this->pixel = pixel;
}

void Imagem::filtroPretoBranco(){
    for (int i = 0; i < this->altura; i++){
        for (int j = 0; j < this->largura; j++){
            this->pixel[i][j].r = (int) ((0.299 * this->pixel[i][j].r) + (0.587 * this->pixel[i][j].g) + (0.144 * this->pixel[i][j].b));
                this->pixel[i][j].g = this->pixel[i][j].r;
                this->pixel[i][j].b = this->pixel[i][j].r;

            if (this->pixel[i][j].r > 255){
              this->pixel[i][j].r = 255;
              this->pixel[i][j].g = 255;
              this->pixel[i][j].b = 255;

            }

        }
    }
} // FONTE: https://www.vivaolinux.com.br/artigo/Manipulacao-de-imagens-no-formato-PPM?pagina=3 -> Rafael Braganca

void Imagem::filtroAutoRelevo(){
        pixels img[this->altura][this->largura]; // cria imagem nova para substituir
         for (int i = 1; i < this->altura - 1; i++){
                         for (int j = 0; j < this->largura; j++){
                                         img[i][j].r = pixel[i + 1][j].r - pixel[i -1][j].r; 
                                         img[i][j].g = pixel[i + 1][j].b - pixel[i -1][j].b;
                                         img[i][j].b = pixel[i + 1][j].b - pixel[i -1][j].b;

                                         if (img[i][j].r < 0) 
                                                         img[i][j].r = 0;

                                         if (img[i][j].g < 0)
                                                         img[i][j].g = 0;

                                         if (img[i][j].b < 0)
                                                         img[i][j].b = 0;
                         }
         }
	 for (int i = 1; i < this->altura - 1; i++){
                         for(int j = 0; j < this->largura; j++){

                                                         pixel[i][j].r = img[i][j].r + 128;
                                                         pixel[i][j].g = img[i][j].g + 128;
                                                         pixel[i][j].b = img[i][j].b + 128;

                                                         if (img[i][j].r > 255)
                                                         img[i][j].r = 255;

                                                         if (img[i][j].g > 255)
                                                         img[i][j].g = 255;

                                                         if (img[i][j].b > 255)
                                                         img[i][j].b = 255;
                         }
         }
}
void Imagem::filtroInversao(){
        pixels img[this->altura][this->largura]; //cria imagem nova para substituir

         for (int i = 0; i < this->altura; i++) {
                         for (int j = 0; j < this->largura; j++) {
                                         img[i][j].r = pixel[i][this->largura - j].r; 
                                         img[i][j].g = pixel[i][this->largura - j].g; 
                                         img[i][j].b = pixel[i][this->largura - j].b; 
                         }
         }
	//substitui
         for (int i = 0; i < this->altura; i++) {
                         for (int j = 0; j < this->largura; j++) {
                                         pixel[i][j].r = img[i][j].r;
                                         pixel[i][j].g = img[i][j].g;
                                         pixel[i][j].b = img[i][j].b;
                         }
         }
}

