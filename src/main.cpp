#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include "../inc/criador.hpp"
#include "../inc/imagem.hpp"

#define MAXIMO 256


using namespace std;


int main(){
        char nomeArquivoExistente[MAXIMO],nomeArquivoNovo[MAXIMO];
        int opcaoMenu,opcaoFiltro;
        Criador criador;
        Imagem* imagem;

        cout << "\n\nMenu: " << endl;
        cout << "\n1- Aplicar Filtro\n2-Fechar programa" << endl;
        cin >> opcaoMenu;

        switch(opcaoMenu){
                case 1:
                        strcpy(nomeArquivoExistente,criador.lerNomeArquivoExistente());
                        imagem = criador.criaImagem(nomeArquivoExistente);
                        cout << "\nEscolha um filto:\n1-Filtro Petro e Branco\n2-Filto alto relevo\n3-Filtro inverter imagem\n" << endl;
                        cin >> opcaoFiltro;
                        switch(opcaoFiltro){
                                case 1:
                                        imagem->filtroPretoBranco();
                                        break;
                                case 2:
                                        imagem->filtroAutoRelevo();
                                        break;
                                case 3:
                                        imagem->filtroInversao();
                                        break;
                        }
                        strcpy(nomeArquivoNovo,criador.lerNomeArquivoNovo());
                        criador.escreveImagem(nomeArquivoNovo,imagem);
                        break;
                case 2:
                       cout << "\nFinalizando o programa.\n\n" << endl;
                        exit(1);
        }
        return 0;
}
                    
