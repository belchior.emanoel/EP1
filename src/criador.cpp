#include "../inc/criador.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <fstream>


#define MAXIMO 256

using namespace std;

char* Criador::lerNomeArquivoExistente(){
        char* nome =(char*)malloc(sizeof(char)*MAXIMO);
        cout << "Digite o nome da imagem: " << endl;
        cin >> nome;
        return nome;
}

char* Criador::lerNomeArquivoNovo(){
        char* nome =(char*)malloc(sizeof(char)*MAXIMO);
        cout << "Digite o nome da imagem com o filtro: " << endl;
        cin >> nome;
	return nome;
}

void Criador::msgErro(){
        cerr << "Digite novamente o nome da imagem seguido de sua extensao .ppm" << endl;
}

Imagem* Criador::criaImagem(char nomeArquivoExistente[MAXIMO]){
        Imagem* imagem;
        ifstream inFile;
        ofstream outFile;
        char nomeAux[MAXIMO];
        int largura,altura,escala;
        pixels** pixel;
        string numero;

        pixel = new pixels* [999420];
        for(int i=0 ; i<999420 ; i++){
                pixel[i] = new pixels[999420];
        }

        strcpy(nomeAux, "doc/");
        strcat(nomeAux, nomeArquivoExistente);
        inFile.open(nomeAux);
        if(inFile.fail()){
                msgErro();
                exit(1);
        }else{
                inFile >> numero >> largura >> altura >> escala;
                for(int i=0 ; i<altura ; i++){
                        for(int j=0 ; j<largura ; j++){
                                inFile >> pixel[i][j].r >> pixel[i][j].g >> pixel[i][j].b;
                        }
                }
                inFile.close();
                 imagem = new Imagem(nomeAux,numero,largura,altura,escala,pixel);
                 return imagem;
        }
}
void Criador::escreveImagem(char nomeArquivoNovo[MAXIMO],Imagem* imagem){
        ofstream outFile;
        char nomeAux[MAXIMO];
        pixels** pixel ;
        pixel = imagem->getPixel();
        strcpy(nomeAux, "doc/");
        strcat(nomeAux, nomeArquivoNovo);
        outFile.open(nomeAux);
        outFile << imagem->getNumero() << endl;
        outFile << imagem->getLargura() << " " << imagem->getAltura() << endl;
        outFile << imagem->getEscala() << endl;
        for(int i=0 ; i<imagem->getAltura() ; i++){
                for(int j=0 ; j<imagem->getLargura() ; j++){
                        outFile << pixel[i][j].r << " " <<  pixel[i][j].g << " " << pixel[i][j].b << endl;
                }
        }
        outFile.close();
        delete(imagem);
        cout << "\nNova imagem criada" << endl;
}

