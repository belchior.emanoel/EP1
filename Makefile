# Executavel
BINFOLDER := bin/
# .hpp
INCFOLDER := inc/
# .cpp
SRCFOLDER := src/
# .o
OBJFOLDER := obj/

CC := g++

CFLAGS := -Wall -ansi -I$(INCFOLDER)

SRCFILES := $(wildcard src/*.cpp)

all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CFLAGS) $(OBJFOLDER)*.o -o $(BINFOLDER)EP1

obj/%.o: src/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

run: 
	$(BINFOLDER)EP1

.PHONY: clean
clean:
	rm -rf bin/*
	rm -rf obj/*
