# EP1 - OO (UnB - Gama)
# Aluno - Emanoel Belchior Elias de Franca
# Matricula - 14/0059733

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

Instrucoes de execucao:

Em um terminal linux com o gitlab instalado e uma conta logada no mesmo, execute os seguintes comandos:

1 - git clone https://gitlab.com/belchior.emanoel/EP1

2 - cd .../EP1

(neste momento mova as imagens que deseja aplicar filtro para a pasta .../doc dentro do repositorio "EP1")

3 - make clean

4 - make

5 - make run

(todas as imagens criadas com filtros serao redirecionadas para a pasta .../doc)
